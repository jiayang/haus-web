import { useState, useEffect } from 'react';
import axios from 'axios';
import EstimationBlock from '../components/EstimationBlock';
import InputField from '../components/InputField';
import ListOptions from '../components/ListOptions';
import Toggle from '../components/Toggle';

const API_URL = process.env.REACT_APP_API_URL;

async function handleClick(isFreehold, selectedStreetName, selectedPropertyType, floorArea, leaseCommenceYear, resaleYear, setEstimation) {
  let data = {
    is_freehold: isFreehold ? 'Yes' : 'No',
    street_name: selectedStreetName,
    property_type: selectedPropertyType,
    floor_area_sqm: Number(floorArea),
    lease_commence_date: Number(leaseCommenceYear),
    resale_date: Number(resaleYear),
  };

  let response = await axios.post(`${API_URL}/api/predict`, data);

  setEstimation(Math.round(response.data.estimation));
}

function Home() {
  const [isFreehold, setIsFreehold] = useState(false);
  const [propertyTypes, setPropertyTypes] = useState([]);
  const [streetNames, setStreetNames] = useState([]);
  const [selectedStreetName, setSelectedStreetNames] = useState('');
  const [selectedPropertyType, setSelectedPropertyTypes] = useState('');
  const [floorArea, setFloorArea] = useState(undefined);
  const [leaseCommenceYear, setLeaseCommenceYear] = useState(undefined);
  const [resaleYear, setResaleYear] = useState(undefined);
  const [estimation, setEstimation] = useState(0);

  useEffect(() => {
    async function fetchData() {
      let response = await axios.get(`${API_URL}/api/options`);

      setPropertyTypes(response.data.propertyTypes);
      setStreetNames(response.data.streetNames);
    }

    fetchData();
  }, []);

  return (
    <div className="bg-gray-100 sm:py-6 lg:py-12 sm:px-6 lg:px-12">
      <div className="bg-white shadow overflow-hidden rounded-lg max-w-xl mx-auto">
        <EstimationBlock estimation={estimation} />

        <div className="px-5 pb-5">
          <ListOptions className="mt-5" label="Street name" options={streetNames} selected={selectedStreetName} onChange={setSelectedStreetNames} />
          <ListOptions className="mt-5" label="Property type" options={propertyTypes} selected={selectedPropertyType} onChange={setSelectedPropertyTypes} />

          <InputField className="mt-5" type="number" label="Floor area" placeholder="60" suffix="sqm" value={floorArea} setState={setFloorArea} />
          <InputField className="mt-5" type="number" label="Lease commence year" placeholder="1970" value={leaseCommenceYear} setState={setLeaseCommenceYear} />
          
          <Toggle className="mt-5" label="Freehold" enabled={isFreehold} onChange={setIsFreehold} />
          
          <InputField className="mt-5" type="number" label="Resale year" placeholder="2038" value={resaleYear} setState={setResaleYear} />

          <button className="w-full mt-8 py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={async () => await handleClick(isFreehold, selectedStreetName, selectedPropertyType, floorArea, leaseCommenceYear, resaleYear, setEstimation)}
          >Estimate</button>
        </div>
      </div>
    </div>
  )
}

export default Home;
