function EstimationBlock({ estimation }) {
  return (
    <div className="px-4 py-5 text-white bg-indigo-600">
      <span className="text-4xl">$</span>
      <span className="text-6xl">{estimation}</span>
    </div>
  );
}

export default EstimationBlock;
