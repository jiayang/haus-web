import { Switch } from '@headlessui/react';

function Toggle({ label, enabled, onChange, className }) {
  return (
    <Switch.Group>
      <div className={className}>
        <Switch.Label passive className="block mb-1 text-sm font-medium text-gray-700">{label}</Switch.Label>
        <Switch
          checked={enabled}
          onChange={onChange}
          className={`${
            enabled ? 'bg-indigo-600' : 'bg-gray-200'
          } relative inline-flex items-center h-9 w-16 rounded-full transition-colors`}
        >
          <span
            className={`${
              enabled ? 'translate-x-8' : 'translate-x-1'
            } inline-block w-7 h-7 transform bg-white rounded-full transition-transform`}
          />
        </Switch>
      </div>
    </Switch.Group>
  )
}

export default Toggle;
