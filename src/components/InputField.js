import PropTypes from 'prop-types';

function InputFieldUnit({ label, align }) {
  return (
    <span className={`${align === 'left' ? 'rounded-l-md border-r-0' : 'rounded-r-md border-l-0'} inline-flex items-center px-3 border border-gray-300 bg-gray-50 text-gray-500 text-sm`}>
      {label}
    </span>
  );
}

function InputField({ type, label, placeholder, prefix, suffix, value, setState, className }) {
  return (
    <div className={className}>
      <label className="block text-sm font-medium text-gray-700">
        {label}
      </label>
      <div className="mt-1 flex rounded-md shadow-md">
        {prefix &&
          <InputFieldUnit label={prefix} align='left' />
        }
        <input
          value={value}
          onChange={(event) => setState(event.target.value)}
          type={type}
          placeholder={placeholder}
          className={`${prefix ? 'rounded-l-none' : ''} ${suffix ? 'rounded-r-none' : ''} focus:ring-2 focus:ring-indigo-500 outline-none py-2 pl-3 w-full rounded-md text-sm border border-gray-300`}
        />
        {suffix &&
          <InputFieldUnit label={suffix} align='right' />
        }
      </div>
    </div>
  )
}

InputFieldUnit.propTypes = {
  label: PropTypes.string.isRequired,
  align: PropTypes.oneOf(['left', 'right']).isRequired,
};

InputField.propTypes = {
  type: PropTypes.oneOf(['text', 'number']).isRequired,
  label: PropTypes.string.isRequired,
  prefix: PropTypes.string,
  suffix: PropTypes.string,
  value: PropTypes.any,
  setState: PropTypes.func.isRequired,
};

InputField.defaultProps = {
  type: 'text',
};

export default InputField;
